from __future__ import division
import pandas as pd
import numpy as np
import ast
import csv
import cPickle
import matplotlib.pyplot as plt
import ast
import collections
import math
import os
import copy


def inDirect():
    child_directory = 'interimFiles'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)

def outDirect():
    os.chdir('..')

def nameSmallest():
    inDirect()
    data = pd.read_csv('beaCodes.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]
    namesCode = collections.OrderedDict({})
    for r in xrange(rows):
        name = data.iloc[r]["Name"]
        code = data.iloc[r]["Code"]
        if type(name) == str and type(code) == str:
            namesCode[code] = name

    data = pd.read_csv('beaIOWeights.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]
    with open('largest.txt', 'w') as file:
        for r in xrange(rows):
            code = data.iloc[r]["bea"]
            name = namesCode[code]
            pair = [code, name]
            pair = ','.join(map(str, pair))
            file.write(pair + '\n')
    outDirect()

def householdFlows():
    # create lists of those sectors to be treated as the household for inflows and outflows
    data = pd.read_csv('householdSectors.csv')
    data = pd.DataFrame(data)
    hhInflows = list(data.iloc[:]["Inflows"].dropna())
    hhOutflows = list(data.iloc[:]["Outflows"].dropna())
    returnDict = {"inflows": hhInflows, "outflows": hhOutflows}
    return returnDict

def otherFlows():
    # create lists of those sectors to be treated as the government, imports and others for inflows and outflows
    data = pd.read_csv('othersSector.csv')
    data = pd.DataFrame(data)
    othersInflows = list(data.iloc[:]["Inflows"].dropna())
    othersOutflows = list(data.iloc[:]["Outflows"].dropna())
    returnDict = {"inflows": othersInflows, "outflows": othersOutflows}
    return returnDict

def buyerSellerValue():
    # rewrite the buyerSellerValue file from the IO table by substituting
    # hhID for sectors which are treated as the household for inflows and outflows
    # and by substituting othersID for sectors which are treated as others for inflows and outflows

    hhID = "hh"
    othersID = "others"
    # the household ID is 0
    hh = householdFlows()
    # get a list of sectors to be treated as the household for inflows and outflows
    inflowHH = hh["inflows"]
    outflowHH = hh["outflows"]

    others = otherFlows()
    inflowOthers = others["inflows"]
    outflowOthers = others["outflows"]

    data = pd.read_csv('IO.csv')
    # load the US inputout table

    data = pd.DataFrame(data)
    rows = data.shape[0]
    heads = list(data.columns.values)
    heads.remove('Code')
    inDirect()
    with open("buyerSellerValue.txt", 'w') as file:
        for r in xrange(rows):
            # for every row in the table
            d = data.iloc[r][:]
            d = d.dropna()
            # drop those entries which are empty
            seller = d[0]
            # seller is the first element of the row
            if seller in inflowHH:
                # if the seller as is treated as a household with regards to inflows,
                # replace the seller with the household
                seller = hhID

            elif seller in inflowOthers:
                seller = othersID

            for buyer in heads:
                # for every buyer in the list of buyers
                if buyer in d:
                    # if the buyer is buying from the agent in the row
                    val = d[buyer]
                    # look up the value
                    val = str(val)
                    val = float(val.replace(',', ''))
                    #val = int(val)
                    # convert the value to an integer
                    if buyer in outflowHH:
                        buyer = hhID
                        # if the buyer is treated as the household with respect to outflows
                        # give the buyer the household ID
                    elif buyer in outflowOthers:
                        buyer = othersID
                    if val != 0:
                        # if the value is not zero
                        info = [buyer, seller, val]
                        # create a string with the ID of the buyer, seller, and the value
                        info = ','.join(map(str, info))
                        file.write(info + '\n')
                        # write it to file, note that negative values are not excluded
    outDirect()

def twoFirms():
    pceSize = collections.OrderedDict({})
    data = pd.read_csv('PCE.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]
    for r in xrange(rows):
        d = data.iloc[r][:]
        bea = d["BEACode"]
        size = d["PCE"]
        if type(size) == str:
            size = size.replace(",", "")
            size = int(size)
            pceSize[bea] = size

    pceProp = collections.OrderedDict({})
    hh = householdFlows()
    hh_inflows = hh["inflows"]
    others = otherFlows()
    others_inflows = others["inflows"]

    inDirect()
    with open("PCESecProp.txt", 'r') as file:
        for line in file:
            line = line.split(",")
            bea = line[0]
            prop = float(line[1])
            pceProp[bea] = prop


    pcePropAdj = collections.OrderedDict({})

    hhtotal = 0
    for bea in hh_inflows:
        if bea in pceSize:
            size = pceSize[bea]
            hhtotal += int(size)
    hhPCE = 0
    for bea in hh_inflows:
        if bea in pceSize:
            size = pceSize[bea]
            p = size/hhtotal
            prop = pceProp[bea]
            hhPCE += prop * p
    pcePropAdj['hh'] = hhPCE

    otherstotal = 0
    for bea in others_inflows:
        if bea in pceSize:
            size = pceSize[bea]
            otherstotal += size
    othersPCE = 0
    for bea in others_inflows:
        if bea in pceSize and bea in pceProp:
            size = pceSize[bea]
            p = size / otherstotal
            prop = pceProp[bea]
            othersPCE += prop * p
    pcePropAdj['others'] = othersPCE

    for bea in pceProp:
        if bea not in hh_inflows and bea not in others_inflows:
            prop = pceProp[bea]
            pcePropAdj[bea] = prop

    allFirms = []
    laborBuyers = []

    with open("buyerSellerValue.txt", 'r') as readFile, open("buyerSellerValue_twoFirms.txt", 'w') as writeFile:
        for line in readFile:
            line = line.split(",")
            buyer = line[0]

            if buyer not in pcePropAdj:
                pcePropAdj[buyer] = 0.0
            seller = line[1]
            #value = int(line[2])
            value = float(line[2])
            sets = []
            if buyer == 'hh' and seller == 'hh':
                set1 = [buyer, seller, value]
                sets.append(set1)
            elif buyer == 'hh' and seller != 'hh':
                sellerC = seller + 'C'
                set1 = [buyer, sellerC, value]
                sets.append(set1)
            elif buyer != 'hh' and seller == 'hh':
                propC = pcePropAdj[buyer]
                if propC >= 1:
                    buyerC = buyer + 'C'
                    set1 = [buyerC, seller, value]
                    sets.append(set1)
                elif propC == 0:
                    buyerI = buyer + 'I'
                    set1 = [buyerI, seller, value]
                    sets.append(set1)
                else:
                    buyerC = buyer + 'C'
                    buyerI = buyer + 'I'
                    valueC = value * propC
                    valueI = value * (1 - propC)
                    set1 = [buyerC, seller, valueC]
                    set2 = [buyerI, seller, valueI]
                    sets.append(set1)
                    sets.append(set2)
            elif buyer != 'hh' and seller != 'hh':
                sellerI = seller + 'I'
                propCSeller = pcePropAdj[seller]
                if propCSeller < 1:
                    propC = pcePropAdj[buyer]
                    if propC >= 1:
                        buyerC = buyer + 'C'
                        set1 = [buyerC, sellerI, value]
                        sets.append(set1)
                    elif propC == 0:
                        buyerI = buyer + 'I'
                        set1 = [buyerI, sellerI, value]
                        sets.append(set1)
                    else:
                        buyerC = buyer + 'C'
                        buyerI = buyer + 'I'
                        valueC = value * propC
                        valueI = value * (1 - propC)
                        set1 = [buyerC, sellerI, valueC]
                        set2 = [buyerI, sellerI, valueI]
                        sets.append(set1)
                        sets.append(set2)

            for item in sets:
                allFirms.append(item[0])
                allFirms.append(item[1])
                if item[1] == 'hh':
                    laborBuyers.append(item[0])
                item = ','.join(map(str, item))
                writeFile.write(item + '\n')
    allFirms = set(allFirms)
    laborBuyers = set(laborBuyers)
    for f in allFirms:
        if f not in laborBuyers:
            print f, 'not labor buyer'
    outDirect()

def network(inputfile):
    # create network
    inDirect()
    allBuyers = []
    allSellers = []
    # create a list of buyers and a list of sellers
    with open(inputfile, 'r') as file:
        # open buyer seller value txt
        for line  in file:
            line = line.split(",")
            buyer = line[0]
            # the first element is buyer
            seller = line[1]
            # the second element is seller

            allBuyers.append(buyer)
            # append buyer to buyers
            allSellers.append(seller)
            # append seller to sellers
    allBuyers = list(set(allBuyers))
    allSellers = list(set(allSellers))
    allAgents = allBuyers + allSellers
    allAgents = list(set(allAgents))
    allAgents.sort()

    # create a list of all agents without repetition
    with open('agents.txt', 'w') as file:
        for a in allAgents:
            file.write(a + '\n')

    networkDict = collections.OrderedDict({})
    # create a dictionary where buyer is the key, and a list of sellers is the value
    for a in allAgents:
        networkDict[a] = []
        # add each agent to networkDict

    # create a network using the list of buyer, seller, value
    with open(inputfile, 'r') as file:
        for line in file:
            line = line.split(",")
            buyer = line[0]
            seller = line[1]
            networkDict[buyer].append(seller) # add sellers to each buyer

    # set the list of sellers, so that each seller is mentioned only once for a buyer
    for k in networkDict:
        sellers = networkDict[k]
        sellers = list(set(sellers))
        sellers.sort()
        networkDict[k] = sellers

    with open("network_raw.txt", 'w') as file:
        # write the network in a file
        for n in networkDict:
            # for each agent
            sellers = networkDict[n]
            # the sellers
            info = [n] + sellers
            # make a list with the agent as the first entry
            info = ','.join(map(str, info))
            file.write(info + '\n')
    outDirect()

def flows(inputfile):
    # use buyer, seller, value to create a similar file but without reptition of links
    # reptition arises because some sectors are bucketed together as household, or others
    inDirect()
    flowDict = collections.OrderedDict({})
    # create a dictionary to store the flows between agents,
    # with the pairs of agents as the key, pair = (buyer, seller), and their link USD as the value
    with open(inputfile, 'r') as file:
        # load the buyerSellerValue file
        for line in file:
            # for each line in the file
            line = line.split(",")
            buyer = line[0]
            # the buyer is the first entry
            seller = line[1]
            # the seller is te second entry
            tup = (buyer, seller)
            # create a tuple (buyer, seller)
            flowDict[tup] = 0
            # set the value of each relation to zero

    with open(inputfile, 'r') as file:
        # open the file
        for line in file:
            line = line.split(",")
            buyer = line[0]
            seller = line[1]
            tup = (buyer, seller)
            qty = float(line[2])
            #qty = int(qty)
            flowDict[tup] += qty
            # add the quantity of their link;
            # this is necessary because some agent relations are mentioned multiple times
            # because the household represents multiple sectors in inflows and outflows

    count = 0
    negativeBuyers = []
    negativeSellers = []
    with open('flow.txt', 'w') as file:
        # open a file
        for pair in flowDict:
            # write the dictionary of linkages
            qty = flowDict[pair]
            buyer = pair[0]
            seller = pair[1]
            if qty < 0:
                count += 1
                negativeBuyers.append(buyer)
                negativeSellers.append(seller)
            info = [buyer, seller, qty]
            info = ','.join(map(str, info))
            file.write(info + '\n')
    #print count, "negative links"
    outDirect()

def flowDifference(flowsDictionary):
    # compute the difference between inflows and outflows of different sectors
    # and returns three dictionaries: inflows, outflows, difference between inflows and outflows
    difference = collections.OrderedDict({})
    inflows = collections.OrderedDict({})
    outflows = collections.OrderedDict({})
    agents = []
    for pair in flowsDictionary:
        buyer = pair[0]
        seller = pair[1]
        agents.append(seller)
        agents.append(buyer)
    agents = list(set(agents))
    for a in agents:
        inflows[a] = 0
        outflows[a] = 0
        difference[a] = 0

    for pair in flowsDictionary:
        buyer = pair[0]
        seller = pair[1]
        qty = flowsDictionary[pair]
        inflows[seller] += qty
        outflows[buyer] += qty
    for a in agents:
        inf = inflows[a]
        outf = outflows[a]
        surplus = inf - outf
        difference[a] = surplus
    return difference, inflows, outflows

def sizes():
    # compute the sizes of the agents
    # in so far as an sectors inflows and outflows are different, adjustments are necessary to fix a size
    # a related adjustment in flows is necessary to make the size distribution invariant
    # Sector X is introduced to create new flows to generate an invariant distribution of sizes

    inDirect()
    agents = []
    flows = collections.OrderedDict({})
    # create a dictionary of the flows; WITH ONLY POSITIVE FLOWS
    # if there is a negative flow, interChange the buyer and seller
    with open('flow.txt', 'r') as file:
        # open flow file
        for line in file:
            # for each line in the file
            line = line.split(",")
            buyer = line[0]  # buyer
            seller = line[1]  # seller
            #qty = int(line[2])  # USD qty of the flow
            qty = float(line[2])  # USD qty of the flow
            if qty > 0:  # if the quantity if positive
                k = (buyer, seller)  # pair is (buyer, seller)
            else:  # if the qty is negative
                k = (seller, buyer)  # interchange buyer and seller
            flows[k] = abs(qty)  # flow is the absolute value of quantity
            agents.append(buyer)  # agents
            agents.append(seller)

    agents = list(set(agents))
    agents.sort()
    # create a list of all agents without repeats
    flowD = flowDifference(flowsDictionary=flows)
    diffDict = flowD[0]  # difference between inflow and outflows of agents
    inflowSizes = flowD[1]  # total inflows of each agent
    outflowSizes = flowD[2]  # total outflows of each agent
    XSectorSize = 0
    XSectorID = "X"
    # the X sector size needs to increased to make additional transfers so that all agents inflows and outflows are equal, and therefore size distribution is invariant and consistent
    # the X sector size must increase by sum of the all positive differences
    # positive difference mean agents inflow is greater than its outflow, this extra from the inflow will go as outflow to the household
    # which the Sector X will give as outflow to other agents who have deficit inflows
    for agent in diffDict:
        surplus = diffDict[agent]
        if surplus > 0:
            XSectorSize += surplus
    print XSectorSize, "Sector X size to match inflow and outflow"

    size = collections.OrderedDict({})
    # create a dictionary to store agent sizes
    # an agents size will be the maximum of its (inflow, outflow)
    # Here is why
    # If inflow is greater than outflow, the agent give the surplus to the household, and its size is the inflow
    # If outflow is greater than inflow, the agent recieves the deficit from the household, and its is the outflow
    for a in agents:
        # for each agent
        IN = inflowSizes[a]
        OUT = outflowSizes[a]
        s = max(IN, OUT)
        # size is the maximum of inflow and outflow
        size[a] = s


    size[XSectorID] = XSectorSize
    # increase the household's size so that it make new transfers to match inflow and outflow of other agents

    with open('sizes_raw.txt', 'w') as file:
        # open a file
        for sec in size:
            # for each sector write its size
            s = size[sec]
            pair = [sec, s]
            pair = ','.join(map(str, pair))
            file.write(pair + '\n')


    # the flows from the Sector X to other sectors must be adjusted so that inflows and outflows are equal for all sectors
    adjustmentFlows = collections.OrderedDict({})
    # uses pairs of (buyer,seller) as key, and the incremental USD flow as value

    for sec in agents:
        # for each sector in agents
        d = diffDict[sec]
        # the surplus of inflows over outflows
        if d > 0:
            pair = (sec, XSectorID)
            # if the surplus is positive
            # the sector is a buyer from the X, outflows from sector to X
            # the X is a seller to the sector; inflows to the X from sector
        elif d < 0:
            pair = (XSectorID, sec)
            # if the surplus is negative
            # the X is a buyer from the sector; outflows from X to the sector
            # the sector is a seller to the X; inflows to the sector from the X
        adjustmentFlows[pair] = abs(d)  # flow is the absolute value of the surplus

    # think more about this adjustment
    # may need to remove some pairs which where in flow
    # may need to add more to flow
    # may need to change the network, in fact it is better to rewrite network using flow information

    for pair in adjustmentFlows:
        # for each pair in adjustment
        qty = adjustmentFlows[pair]
        # the quantity of adjustment
        if pair in flows:
            # if the pair is in the flows dictionary
            flows[pair] += qty
            # add the quatity
        else:
            flows[pair] = qty
            # else add the pair and the quantity

    def checkNegative():
        count = 0
        negativePairs = []
        for pair in flows:
            qty = flows[pair]
            if qty < 0:
                count += 1
                negativePairs.append(pair)
        #print count, "negative links"
        flowD = flowDifference(flowsDictionary=flows)
        diffDict = flowD[0]
        #print sum(diffDict.values()), "difference in inflow and outflow sizes of all agents together"

    checkNegative()

    with open('completeFlows.txt', 'w') as file:
        for pair in flows:
            siz = flows[pair]
            if int(siz) > 0:
                info = list(pair) + [siz]
                info = ','.join(map(str, info))
                file.write(info + '\n')

    networkDict = {}
    for a in agents:
        networkDict[a] = []
    networkDict['X'] = []
    for pair in flows:
        buyer = pair[0]
        seller = pair[1]
        networkDict[buyer].append(seller)

    for buyer in networkDict:
        sellers = networkDict[buyer]
        if len(sellers) != len(set(sellers)):
            print "repeats in sellers"


    with open('completeNetwork.txt', 'w') as file:
        for buyer in networkDict:
            sellers = networkDict[buyer]
            info = [buyer] + sellers
            info = ','.join(map(str, info))
            file.write(info + '\n')

    # the quantity of flows from buyer to each of its seller
    # it a dictionary of dictionaries, for each buyer there is a dictionary of sellers and the flow to those sellers
    buyerSellerQty = collections.OrderedDict({})
    for pair in flows:
        s = flows[pair]
        buyer = pair[0]
        seller = pair[1]
        if buyer not in buyerSellerQty:
            buyerSellerQty[buyer] = collections.OrderedDict({})
        buyerSellerQty[buyer][seller] = s

    # what is the total flows from each sector to non-X
    buyerTotalFlows = {}
    buyerNonXFlows = {}
    for buyer in buyerSellerQty:
        total = sum(buyerSellerQty[buyer].values())
        buyerTotalFlows[buyer] = total
        if XSectorID in buyerSellerQty[buyer].keys():
            Xflow = buyerSellerQty[buyer][XSectorID]
            buyerNonXFlows[buyer] = (total-Xflow)
        else:
            buyerNonXFlows[buyer] = total

    buyerXShare = {}
    # the share of flows to X as a proportion of all flows for each sector
    for buyer in buyerTotalFlows:
        total = buyerTotalFlows[buyer]
        nonX = buyerNonXFlows[buyer]
        buyerXShare[buyer] = (1 - nonX/total)

    # nonX weights
    buyerNonXWeights = {}
    for buyer in buyerNonXFlows:
        #print buyer, "buyer"
        nonX_total = buyerNonXFlows[buyer]
        if buyer not in buyerNonXWeights:
            buyerNonXWeights[buyer] = {}
        for seller in buyerSellerQty[buyer]:
            if seller == 'X':
                pass
            else:
                #print "buyer:", buyer, "seller:", seller
                flow = buyerSellerQty[buyer][seller]
                weight = flow / nonX_total
                buyerNonXWeights[buyer][seller] = weight

    # normalize the nonX weights to their share, so as to maintain the relative flows to different sectors
    for buyer in buyerNonXWeights:
        if buyerNonXFlows[buyer] > 0:
            total = sum(buyerNonXWeights[buyer].values())
            share = 1 - buyerXShare[buyer]
            multiple = share/total
            for seller in buyerNonXWeights[buyer]:
                weight = buyerNonXWeights[buyer][seller]
                weight *= multiple
                buyerNonXWeights[buyer][seller] = weight
        else:
            buyerNonXWeights[buyer][seller] = 0

    # X weights
    buyerXWeights =  {}
    for buyer in buyerTotalFlows:
        total = buyerTotalFlows[buyer]
        if buyer in buyerNonXFlows:
            weight = 1 - (buyerNonXFlows[buyer]/total)
        else:
            weight = 0
        buyerXWeights[buyer] = weight

    weights = {}

    for buyer in buyerSellerQty:
        if buyer not in weights:
            weights[buyer] = {}
        for seller in buyerSellerQty[buyer]:
            if seller == XSectorID:
                w = buyerXWeights[buyer]
            else:
                w = buyerNonXWeights[buyer][seller]
            w = float(w)
            weights[buyer][seller] = w

    with open('weights_raw.txt', 'w') as file:
        for buyer in weights:
            for seller in weights[buyer]:
                w = weights[buyer][seller]
                info = [buyer, seller, w]
                info = ','.join(map(str, info))
                file.write(info + '\n')
    outDirect()

def index(twoFirms):
    inDirect()
    # index the network and weights files
    # sectors go from 1 to n, where n is the number of non-household sector
    # 0 is the index of household
    agents = []
    with open("agents.txt", 'r') as file:
        for line in file:
            sec = line.rstrip('\n')
            agents.append(sec)
    list(set(agents)).sort()
    networkDict = {}
    with open("completeNetwork.txt", 'r') as file:
        for line in file:
            line = line.split(",")
            last = line[-1]
            last = last.rstrip('\n')
            line[-1] = last
            buyer = line[0]
            sellers = line[1:]
            networkDict[buyer] = sellers

    indexDictionary = collections.OrderedDict({})
    agents.remove('hh')  # remove the household from agents
    if twoFirms:
        agents.remove('othersC')
        agents.remove('othersI')
    else:
        agents.remove('others')
    indexes = range(1, len(agents) + 1)  # the indexes of all other agents
    n = len(agents)
    indexDictionary['hh'] = 0  # assign the household index 0
    if twoFirms:
        indexDictionary['othersC'] = n + 1  # assign 'others' sector index
        indexDictionary['othersI'] = n + 2  # assign 'others' sector index
        indexDictionary['X'] = n + 3  # assign X index
    else:
        indexDictionary['others'] = n + 1  # assign 'others' sector index
        indexDictionary['X'] = n + 2  # assign X index



    for i in xrange(len(agents)):  # assign each agent an integer
        ind = indexes[i]
        agent = agents[i]
        indexDictionary[agent] = ind

    with open('index.txt', 'w') as file, open('C_index.txt','w') as Cfile:
        # write the indexes in a file
        for sec in indexDictionary:
            ind = indexDictionary[sec]
            pair = [sec, ind]  # sector and index
            pair = ','.join(map(str, pair))
            file.write(pair + '\n')
            if sec[-1] == 'C':
                Cfile.write(str(ind) + '\n')

    networkIndex = collections.OrderedDict({})
    for buyer in networkDict:  # for each buyer in the network
        buyerIndex = indexDictionary[buyer]  # the index of the buyer
        sellers = networkDict[buyer]  # the sellers are
        sellersIndex = []  # create a list of the sellers index
        for s in sellers:  # for each seller
            sInd = indexDictionary[s]  # the index of the seller is
            sellersIndex.append(sInd)  # add the index to the sellers list
        sellersIndex.sort()  # sort the list according to index
        networkIndex[buyerIndex] = sellersIndex  # add the indexed buyers and sellers to the network

    countLabor = 0  # number of labor buyers
    countGoods = 0  # number of goods sellers
    count = 0  # all agents
    laborIDS = []
    goodsIDS = []
    allIDS = []
    with open('network.txt', 'w') as networkFile, open('laborBuyers.txt', 'w') as laborFile, open(
            'goodsSellers.txt', 'w') as goodsFile:
        # networkFile has firm to firm network, where firm is a non-household sector
        # laborFile has all the sectors which buy from the household
        # goodsSellers has all the sectors which seller to the household
        laborFile.write(str(0) + '\n')
        countLabor += 1
        for buyer in networkIndex:  # for each buyer
            allIDS.append(buyer)
            count += 1
            sellers = copy.deepcopy(networkIndex[buyer])  # the sellers
            if buyer != 0:
                if 0 in sellers:
                    laborFile.write(str(buyer) + '\n')
                    countLabor += 1
                    sellers.remove(0)
                    laborIDS.append(buyer)
                info = [buyer] + sellers  # list of buyer + its sellers
                info = ','.join(map(str, info))
                networkFile.write(info + '\n')
            else:
                for s in sellers:
                    goodsFile.write(str(s) + '\n')
                    countGoods += 1
                    goodsIDS.append(s)

    #print count, "all"
    #print countLabor, "labor buyers"
    #print countGoods, "goods sellers"

    nonLabor = []
    for i in allIDS:
        if i not in laborIDS:
            nonLabor.append(i)

    nonGoods = []
    for i in allIDS:
        if i not in goodsIDS:
            nonGoods.append(i)

    #print nonLabor, "non labor"


    with open('weights_raw.txt', 'r') as file, open('weights.txt', 'w') as indFile:
        # write the weights of linkages between agents using indexes
        for line in file:  # for each line in the weights file
            line = line.split(",")
            buyer = line[0]  # buyer is the first entry
            seller = line[1]  # seller is the next entry
            weight = float(line[2])  # weight is the last entry
            indBuyer = indexDictionary[buyer]  # index of the buyer
            indSeller = indexDictionary[seller]  # index of the seller
            info = [indBuyer, indSeller, weight]  # new info
            info = ','.join(map(str, info))
            indFile.write(info + '\n')

    with open("sizes_raw.txt", 'r') as file, open("sizes.txt", 'w') as indFile:
        for line in file:
            line = line.split(",")
            agent = line[0]
            #s = int(line[1])
            s = float(line[1])
            agentInd = indexDictionary[agent]
            info = [agentInd, s]
            info = ','.join(map(str, info))
            indFile.write(info + '\n')

    with open('largest.txt', 'r') as file, open("largestInd.txt", 'w') as indFile:
        count = 1
        for line in file:
            line = line.split(",")
            ID = line[0]
            if ID in indexDictionary:
                ind = indexDictionary[ID]
            else:
                ind = None

            pair = [ID, ind, count]
            pair = ','.join(map(str, pair))
            indFile.write(pair + '\n')
            count += 1
    outDirect()

def dashCodes(code):
    dash = "-"
    if dash in code:
        codes = code.split("-")
        begin = codes[0]
        endDigit = codes[1]
        lastDigit_begin = begin[-1]
        if lastDigit_begin >= endDigit:
            #print code, 'code'
            #print "begin more than end"
            return
        numCodes = int(endDigit) - int(lastDigit_begin) + 1
        codes = []
        for n in range(numCodes):
            code = copy.copy(begin)
            code = code[:-1]
            code += str(int(lastDigit_begin) + n)
            codes.append(code)
    else:
        codes = [code]
    return codes

def beaNaics():
    # write the NAICS codes pertaining to each BEA Code in the IO Table
    data = pd.read_csv('raw_beaNaics.csv')
    # load the US inputout table
    data = pd.DataFrame(data)
    rows = data.shape[0]
    heads = list(data.columns.values)
    codesDict = collections.OrderedDict({})
    for r in xrange(rows):
        d = data.iloc[r][:]
        naicsCodes = d["NAICS"]
        beaCode = d["BEA"]
        comma = ","
        allCodes = []
        if comma in naicsCodes:
            naicsCodes = naicsCodes.split(",")
            for code in naicsCodes:
                codes = dashCodes(code)
                allCodes += codes
        else:
            codes = dashCodes(naicsCodes)
            allCodes += codes
        codesDict[beaCode] = allCodes
    inDirect()

    with open('beaNaics.txt', 'w') as file:
        for bea in codesDict:
            naics = codesDict[bea]
            if naics[0] == 'n/a':
                naics = [None]
            info = [bea] + naics
            info = ','.join(map(str, info))
            file.write(info + '\n')

    allNaicsCodes = []
    for bea in codesDict:
        naics = codesDict[bea]
        if naics[0] == 'n/a':
            pass
        else:
            allNaicsCodes += naics
    allNaicsCodes = [int(i) for i in allNaicsCodes]
    allNaicsCodes = list(set(allNaicsCodes))
    allNaicsCodes.sort()
    with open('beaNaicsList.txt', 'w') as file:
        for code in allNaicsCodes:
            file.write(str(code) + '\n')

    outDirect()

def IntKnown(v):
    a = 0
    dash = '-'
    if dash in v:
        a = None
    elif isinstance(v, int):
        a += int(v)
    elif isinstance(v, float):
        if not math.isnan(v):
            a += int(v)
    else:
        if v.isdigit():
            a += int(v)
    return a

def naicsSizesWeights():
    data = pd.read_csv('naicsSizes.csv') # file with the sizes of all naics sectors
    data = pd.DataFrame(data)
    heads = list(data.columns.values)
    rows = data.shape[0]
    all = collections.OrderedDict({})
    small0 = collections.OrderedDict({})
    small1 = collections.OrderedDict({})
    small10 = collections.OrderedDict({})
    for r in xrange(rows):
        d = data.iloc[r]
        naics = d["naics"]
        naics = IntKnown(naics)
        if naics != None:
            d_all = d['all']
            d_small0 = d['small0']
            d_small1 = d['small1']
            d_small10 = d['small10']
            all[naics] = d_all
            small0[naics] = d_small0
            small1[naics] = d_small1
            small10[naics] = d_small10

    inDirect()

    naics = []
    with open("beaNaicsList.txt", 'r') as file:
        for line in file:
            ID = int(line)
            naics.append(ID)

    naics_all = collections.OrderedDict({})
    naics_small0 = collections.OrderedDict({})
    naics_small1 = collections.OrderedDict({})
    naics_small10 = collections.OrderedDict({})

    missingMatch = {}
    with open ("naicsMissingCodeMatch.txt", 'r') as file:
        for line in file:
            line = line.split(",")
            IO_naics = line[0]
            naicsMatch = line[1]
            naicsMatch = naicsMatch.rstrip('\n')
            if naicsMatch != "None":
                IO_naics = int(IO_naics)
                naicsMatch = int(naicsMatch)
                missingMatch[IO_naics] = naicsMatch

    weightsDivNAICS11 = {}
    a11 = []
    sizesCodes = []
    with open ("weightsNaicsCodeDivision.txt", 'r') as file:
        for line in file:
            line = line.split(",")
            code = line[0]
            code = int(code)
            w = float(line[1])
            weightsDivNAICS11[code] = w
            a11.append(code)

    for code in naics:
        exists = True
        if code not in all:
            if code not in missingMatch:
                exists = False

        if exists:
            sizesCodes.append(code)
            if code in missingMatch:
                modCode = missingMatch[code]
            else:
                modCode = copy.copy(code)

            if code in a11:
                w = weightsDivNAICS11[code]
                code_all = w * all[modCode]
                code_small0 = w * small0[modCode]
                code_small1 = w * small1[modCode]
                code_small10 = w * small10[modCode]
            else:
                code_all = all[modCode]
                code_small0 = small0[modCode]
                code_small1 = small1[modCode]
                code_small10 = small10[modCode]

            naics_all[code] = code_all
            naics_small0[code] = code_small0
            naics_small1[code] = code_small1
            naics_small10[code] = code_small10


    file_name = "naicsIOSizes"
    allTotal = 0
    s0Total = 0
    s1Total = 0
    s10Total = 0

    with open('%s.csv' % file_name, 'wb') as data_csv:
        writer_data = csv.writer(data_csv, delimiter=',')
        writer_data.writerow(["naics", "all", "s0", "s1", "s10"])
        for n in sizesCodes:
            writer_data.writerow([n, naics_all[n], naics_small0[n], naics_small1[n], naics_small10[n]])
            allTotal += naics_all[n]
            s0Total += naics_small0[n]
            s1Total += naics_small1[n]
            s10Total += naics_small10[n]


    with open("naicsIOWeights.csv", 'wb') as dataCSVWeights:
        writerDataWeights = csv.writer(dataCSVWeights, delimiter=',')
        writerDataWeights.writerow(["naics", "all", "s0", "s1", "s10"])
        for n in sizesCodes:
            """
            writerDataWeights.writerow([n,
                                  naics_all[n]/allTotal,
                                  naics_small0[n]/s0Total,
                                  naics_small1[n]/s1Total,
                                  naics_small10[n]/s10Total]
            """
            writerDataWeights.writerow([n, naics_all[n]/naics_all[n],
                                            naics_small0[n]/naics_all[n],
                                            naics_small1[n]/naics_all[n],
                                            naics_small10[n]/naics_all[n]])

    outDirect()

def beaWeights(two_firms):
    inDirect()
    # converts naics weights to bea weights, some weights are added, some are divided
    # in particular weights pertaining to 23 are divided into several bea codes

    beaNaics = collections.OrderedDict({})
    naicsBea = collections.OrderedDict({})
    with open("beaNaics.txt", 'r') as file:
        for line in file:
            line = line.split(",")
            bea = line[0]
            naics = line[1:]
            if bea != "814000":
                if naics[-1].rstrip('\n') != "None":
                    naics = [int(c) for c in naics]
                    beaNaics[bea] = naics
                    for n in naics:
                        if n in naicsBea:
                            naicsBea[n].append(bea)
                        else:
                            naicsBea[n] = [bea]

    multipleNaicsBea = {}
    for naics in naicsBea:
        bea = naicsBea[naics]
        if len(bea) > 1:
            multipleNaicsBea[naics] = bea

    beaWeightsAll = collections.OrderedDict({})
    beaWeightsS0 = collections.OrderedDict({})
    beaWeightsS1 = collections.OrderedDict({})
    beaWeightsS10 = collections.OrderedDict({})

    for bea in beaNaics:
        beaWeightsAll[bea] = 0
        beaWeightsS0[bea] = 0
        beaWeightsS1[bea] = 0
        beaWeightsS10[bea] = 0

    data = pd.read_csv('naicsIOWeights.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]

    for r in xrange(rows):
        d = data.iloc[r]
        naics = int(d["naics"])
        all = d["all"]
        s0 = d["s0"]
        s1 = d["s1"]
        s10 = d["s10"]
        if naics not in multipleNaicsBea:
            bea = naicsBea[naics]
            if len(bea) == 1:
                bea = bea[0]
                beaWeightsAll[bea] += all
                beaWeightsS0[bea] += s0
                beaWeightsS1[bea] += s1
                beaWeightsS10[bea] += s10
        else:
            print naics, "naics in multiple"
            manyBea = multipleNaicsBea[naics]
            n = len(manyBea)
            for mBea in manyBea:
                beaWeightsAll[mBea] += (all/n)
                beaWeightsS0[mBea] += (s0/n)
                beaWeightsS1[mBea] += (s1/n)
                beaWeightsS10[mBea] += (s10/n)

    index = collections.OrderedDict({})
    with open('index.txt', 'r') as file:
        for line in file:
            line = line.split(',')
            bea = line[0]
            ind = int(line[1])
            index[bea] = ind

    sizes = {}
    with open('sizes_raw.txt', 'r') as file:
        for line in file:
            line = line.split(",")
            code = line[0]
            #size = int(line[1])
            size = float(line[1])
            sizes[code] = size

    file_name = 'beaIOWeights'
    indexfile_name = 'indexBeaIOWeights'
    hh = {'all':0, 's0':0, 's1':0, 's10':0}
    with open('%s.csv' % file_name, 'wb') as data_csv, open('%s.csv' % indexfile_name, 'wb') as indexData_csv, \
            open('smallFirmsRatio.csv', 'wb') as small:
        writer_data = csv.writer(data_csv, delimiter=',')
        indexWriter_data = csv.writer(indexData_csv, delimiter=',')
        smallWriter = csv.writer(small, delimiter=',')
        writer_data.writerow(["bea", "all", "s0", "s1", "s10"])
        indexWriter_data.writerow(["index", "all", "s0", "s1", "s10"])
        smallWriter.writerow(["index", "all", "s0", "s1", "s10"])
        for n in beaWeightsAll:
            writer_data.writerow([n, beaWeightsAll[n], beaWeightsS0[n], beaWeightsS1[n], beaWeightsS10[n]])
            if two_firms:
                nC = n + 'C'
                nI = n + 'I'
                if nC in sizes and nI in sizes:
                    sC = sizes[nC]
                    sI = sizes[nI]
                    propC = sC / (sC + sI)
                    propI = 1 - propC
                    indC = index[nC]
                    indI = index[nI]
                    indexWriter_data.writerow([indC, propC * beaWeightsAll[n], propC * beaWeightsS0[n], propC * beaWeightsS1[n], propC * beaWeightsS10[n]])
                    indexWriter_data.writerow([indI, propI * beaWeightsAll[n], propI * beaWeightsS0[n], propI * beaWeightsS1[n], propI * beaWeightsS10[n]])
                    if beaWeightsAll[n] > 0:
                        smallWriter.writerow([indC, beaWeightsAll[n] / beaWeightsAll[n], beaWeightsS0[n] / beaWeightsAll[n],
                             beaWeightsS1[n] / beaWeightsAll[n], beaWeightsS10[n] / beaWeightsAll[n]])
                        smallWriter.writerow([indI, beaWeightsAll[n] / beaWeightsAll[n], beaWeightsS0[n] / beaWeightsAll[n],
                             beaWeightsS1[n] / beaWeightsAll[n], beaWeightsS10[n] / beaWeightsAll[n]])
                elif nC in sizes:
                    indC = index[nC]
                    indexWriter_data.writerow([indC, beaWeightsAll[n], beaWeightsS0[n], beaWeightsS1[n], beaWeightsS10[n]])
                    if beaWeightsAll[n] > 0:
                        smallWriter.writerow([indC, beaWeightsAll[n] / beaWeightsAll[n], beaWeightsS0[n] / beaWeightsAll[n],
                             beaWeightsS1[n] / beaWeightsAll[n], beaWeightsS10[n] / beaWeightsAll[n]])
                elif nI in sizes:
                    indI = index[nI]
                    indexWriter_data.writerow([indI, beaWeightsAll[n], beaWeightsS0[n], beaWeightsS1[n], beaWeightsS10[n]])
                    if beaWeightsAll[n] > 0:
                        smallWriter.writerow([indI, beaWeightsAll[n] / beaWeightsAll[n], beaWeightsS0[n] / beaWeightsAll[n],
                             beaWeightsS1[n] / beaWeightsAll[n], beaWeightsS10[n] / beaWeightsAll[n]])
                elif n in sizes:
                    ind = index[n]
                    indexWriter_data.writerow([ind, beaWeightsAll[n], beaWeightsS0[n], beaWeightsS1[n], beaWeightsS10[n]])
                    if beaWeightsAll[n] > 0:
                        smallWriter.writerow([ind, beaWeightsAll[n] / beaWeightsAll[n], beaWeightsS0[n] / beaWeightsAll[n],
                             beaWeightsS1[n] / beaWeightsAll[n], beaWeightsS10[n] / beaWeightsAll[n]])
            else:
                ind = index[n]
                indexWriter_data.writerow([ind, beaWeightsAll[n], beaWeightsS0[n], beaWeightsS1[n], beaWeightsS10[n]])
                if beaWeightsAll[n]>0:
                    smallWriter.writerow([ind, beaWeightsAll[n]/beaWeightsAll[n], beaWeightsS0[n]/beaWeightsAll[n], beaWeightsS1[n]/beaWeightsAll[n], beaWeightsS10[n]/beaWeightsAll[n]])
                else:
                    print n, "zero size"
    outDirect()

def indexPCE(twoFirms): #using PCE file compute propotion of PCE expenditure going to specific sectors
    beaPCEWeight = collections.OrderedDict({})
    data = pd.read_csv('PCE.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]
    for r in xrange(rows):
        bea = data.iloc[r]["BEACode"]
        weight = data.iloc[r]["weight"]
        if twoFirms:
            #print bea
            bea = bea + 'C'
        beaPCEWeight[bea] = weight
    inDirect()
    index = collections.OrderedDict({})
    with open('index.txt', 'r') as file:
        for line in file:
            line = line.split(",")
            bea = line[0]
            ind = int(line[1])
            index[bea] = ind
    if str(0) in index:
        del index[str(0)]


    indexPCEWeight = collections.OrderedDict({})


    for beaCode in beaPCEWeight:
        weight = beaPCEWeight[beaCode]
        if weight > 0:
            if beaCode in index:
                ind = index[beaCode]
                weight = beaPCEWeight[beaCode]
                indexPCEWeight[ind] = weight


    # normalize the weights, they are about 97% without normalization
    totalWeights = sum(indexPCEWeight.values())
    for ind in indexPCEWeight:
        indexPCEWeight[ind] /= totalWeights
    with open('PCE_weights.txt', 'w') as file:
        for ind in indexPCEWeight:
            weight = indexPCEWeight[ind]
            pair = [ind, weight]
            pair = ','.join(map(str, pair))
            file.write(pair + '\n')


    outDirect()

def pceSectors(): # use a file of PCE sectors, write a file with proportion of those sectors spend on PCE
    data = pd.read_csv('PCE.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]
    inDirect()
    with open('PCESecProp.txt', 'w') as file:
        for r in xrange(rows):
            d = data.iloc[r][:]
            bea = d["BEACode"]
            prop = d["pceProportion"]
            if prop != "#DIV/0!":
                prop = float(prop)
                propAdj = min(prop, 1)
            else:
                propAdj = 0.0
            pair = [bea, propAdj]
            pair = ','.join(map(str, pair))
            file.write(pair + '\n')

    outDirect()

pceSectors() # write the share of each sector in PCE as a txt using PCE.csv
buyerSellerValue() # rewrite buyerSellerValue_twoFirms so that household sectors are represented by code hh, others are code "others
twoFirms() # use buyerSellerValue.txt to create buyerSellerValue_twoFirms.txt
network(inputfile="buyerSellerValue_twoFirms.txt") # write network from (buyer, seller, value)
flows(inputfile="buyerSellerValue_twoFirms.txt") # create buyer,seller,value file without repetition; reptition arises because some sectors are treated as "others"
sizes() # compute the sizes of different sectors
index(twoFirms=True) # index the network, weights files

beaNaics() # use IO table description to write NAICS codes associated with different BEA codes
naicsSizesWeights() # write sizes of those NAICS codes which appear in the IO table
beaWeights(two_firms=True) # convert NAICS weights to BEA weights
indexPCE(twoFirms=True)

