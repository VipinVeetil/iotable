
inputOutput.py uses data on the US IO table to generate the following files:
Each of the following files is indexed so that sectors run from 1 to the number of sectors, and the household has index 0

OUTPUT FILES

(1) network.txt;  the buyer-seller relations between different sectors of the US economy
(2) weights.txt; the outflow of money from each sector to other sectors; where the weights  sum to one for each sector
(3) size.txt; the size of each sector after certain adjustments
(4) PCE_weights.txt; the share of each in PCE Index
(5) smallFirmsRatio.csv; the share of small firms in each sector, where small is defined by firms with revenues less than one hundred thousand (s0), one million (s1), and ten million (s10).

Files (1) and (2) are used to construct the network. File (3) is used to initialize the economy. File (4) is used to compute the PCE Index. File (5) determines the proportion in 

INPUT FILES

IO_US.csv  has the US Input Output table from 2007

householdSectors.csv contains sectors which shall be treated as the representative house. The file has two sets of sectors considered as the household, one for inflow and another for outflows. The inflows and outflows differ because there are some sectors which appear as inflows in the IO Table but not as outflows, and vice-versa.

othersSectors.csv contains sectors which shall be treated as "others" sector.

PCE.csv share of each sector in the PCE Index.
PCE.csv is games as PCE.xlsx but in csv format. PCE.xlsx is created using the IO table. Column A has BEACode, Column B has the sector description. Column C has the PCE. Column E is weight as computed by dividing the PCE of each sector by the total PCE. Column F has the total commodity value of the sector. And Column G 'pceProportion' is the ratio of PCE of each sector to its total commodity production. Scrap and imports are excluded in PCE files because they are negative.



FUNCTIONS

pceSectors() uses PCE.csv to write a txt file with the share of each sector in PCE Index

householdFlows() loads the householdSectors.csv file, and returns a dictionary with lists of those sectors to be treated as household for inflows and outflows.

otherFlows() loads othersSectors.csv file, and returns a dictionary with lists of those sectors to be treated as others for inflows and outflows.

buyerSellerValue() write the network with one line for each relation, [buyer, seller, value]. All non-zero values are written, including negative values. The negative values comes from negative entries in the US IO Table.

twoFirms() divides sectors in two firms. One firm sells to the household, another firms sells to all others sectors. They are denoted by ending with 'C' and 'I' respectively. If a sector sells only to the household, 'I' sector is not created. Similarly, if a sector sells only to non-households, 'C' sector is not created. The function uses buyerSellerValue file to 


network() uses  buyerSellerValue.txt to write network. The network is written in the format that each line has one buyer as the first entry, followed by all its sellers. There are no repeat sellers. And all agents in the network have one and only one line.

flows() uses buyerSellerValue.txt to write the size of flows between each agent. buyerSellerValue.txt has double entries (A,A, and A,A) because the household represents multiple sectors. The flows.txt file sum multiple entries, so each line has a unique (buyer, seller) pair and the value of the sum of the relation to each other. Negative entries are not exclude. There are 41 negative links, with the household being the buyer in all but two of the cases.

sizes() generates an invariant distribution of sizes of all agents in the economy. It also generates the weights between links in the network. An adjustment of agent sizes and flows is necessary to generate an invariant distribution. Sector X is introduced as the free variable which incurs additional inflows and outflows necessary to generate an invariant distribution of sizes. In the case of the 41 negative linkages, the buyer and seller were interchanged, and the flow was made positive.

index() indexes the network and correspondingly the weights of linkages, with integers from 0 to n, where is the n number of non-household sectors, 0 is the index of the household.

---------------------------------
THIS COMPLETES THE CREATION OF THE NETWORK, WEIGHTS, SIZE, AND PCE.
WE MOVE TO THE CREATION OF smallFirmsRatio.csv, WHICH GIVES THE RATIOS OF SMALL FIRMS IN EACH SECTOR, AND THEREFORE THE PROPORTION IN WHICH THE MONETARY SHOCK IS DIVIDED ACROSS THE SECTORS.
---------------------------------
The distribution of firms size across sectors is available from the Census based on NAICS codes. We need to transform these into sizes as per the codes in IO tables (and then into the our indexes). The IO table presents a mapping from the BEA IO Codes to the NAICS codes. However, there are some sectors which appear in the IO table but not appear in the NAICS size distribution. These can be divided into three groups. 
Group 1: There is no match in the NAICS distribution
Group 2: There is a unique one to one alternate match found through the NAICS website.
Group 3: There is a match in the NAICS distribution, but it matches to several of the unmatched and therefore must be divided up among these, in which case the weights of these division needs to be gotten from the sizes of these subsections in the IO table.

More specifically, following are the entries of each Group.

Group 1 NO MATCH

482 Railway transport
491 Postal Service
814 Private HH
51913 Internet publishing and broadcasting and Web search portals
5719 Satellite, telecommunications resellers, and all other telecommunications


Group 2 (IO_NAICS = NAICS) PERFECT MATCH


33461 (Manufacturing and reproducing magnetic and optical media) = 334613 (Magnetic and Optical Recording Media Manufacturing)

51121 = 5112 (Software Publishers)



Group 3 (All belong to 11) MATCH WITH A SUPER ITEM, WHICH NEEDS TO BE DIVIDED
1112-4
1119
1122-5
1129

11111-6
11119
11211-3

The above information is stored in a file named naicsMissingCodeMatch.txt
---------------------------------------------------------------------

INPUT FILE

raw_beaNaics.csv file is created using the BEA-NAICS conversion presented in a file of the  US IO Table.

naicsSizes.csv file contains information on the total receipts of firms of three sizes in all sectors. The sizes are firms with receipts of less than hundred thousand, one million, and ten million. The file is take after processing the NAICS size distribution in another folder.

naicsMissingCodeMatch.txt

weightsNaicsCodeDivision.txt


FUNCTIONS

beaNaics() 
This function writes three files
(1) The NAICS codes for each BEA Code using the conversion table and manual conversions stated before in a file named beaNaics.txt Each line of the txt file begins with a BEA Code, followed by all the NAICS codes comma separated. 
(2) beaNAICSList.txt which lists all the NAICS codes which pertain to the BEA Codes in the IO Table. 
(3) naicsIOWeights.csv which has the share of small firms by NAICS codes for those the NAICS codes pertaining to BEA Codes in the IO Table.

beaWeights()
Uses information on the size distribution with NAICS codes to write the file smallFirmsRatio. Functions converts distribution by NAICS codes to distribution by BEA codes, and then indexes, to write smallFirmsRatio.csv.

 




